# RATIONALE #

The only purpose of this repository is just only one: a digital repo for our digitized publications / books. 

### What is this repository for? ###

* Quick summary
    - Digital repository of digitial assets 
* Version 1.01

### How do I get set up? ###

* Summary of set up
	- a stable operating system installed
	- a pdf reader
* Configuration
    - 
* Dependencies
    - There is no dependencies
* Database configuration
    - There is no databases to share
* How to run tests
    - There is no tests to run
* Deployment instructions
	- At your left, go to the [Downloads](https://bitbucket.org/digital_repository/imhicihu-digital-repository/downloads/) section. Browse the content and pick your publication to download.  


### Changelog ###

* Please check the [Commits](https://bitbucket.org/digital_repository/imhicihu-digital-repository/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - There is no writing test to face
* Code review
    - There is no code to run
* Other guidelines
    - There is no guidelines. But you can browse the `Source` section

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/XXXXXXXXXXXX/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)


### Legal ###

* All trademarks are the property of their respective owners.

### Copyright ###
![88x31.png](https://bitbucket.org/repo/4pKrXRd/images/3902704043-88x31.png)
This work is licensed under a [Creative Commons Attribution-ShareAlike 2.0 Generic License](http://creativecommons.org/licenses/by-sa/2.0/).